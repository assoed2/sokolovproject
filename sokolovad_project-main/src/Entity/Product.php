<?php
namespace App\Entity;

class Product
{
    public string $name;
    public string $vendorCode;
    public string $price;
    public string $description;
    public int $id;
    public int $warestockAmount;
    public int $status;
    public function __construct($id,$name,$vendorCode,$price,$description,$warestockAmount,$status){
        $this->id = $id;
        $this->name = $name;
        $this->vendorCode = $vendorCode;
        $this->price = $price;
        $this->description = $description;
        $this->warestockAmount = $warestockAmount;
        $this->status = $status;
    }
}