<?php
namespace App\Responses;

use http\Message;

class ProductResponse
{
    public function __construct(int $code,string $message,array $data){
        $this->code = $code;
        $this->message = $message;
        $this->data = $data;
    }
}