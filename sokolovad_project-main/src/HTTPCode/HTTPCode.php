<?php
namespace App\HTTPCode;

class HTTPCode
{
    public const NOT_FOUND= 404;
    public const BAD_REQUEST = 400;
    public const GOOD_REQUEST = 200;
    public const CREATED = 201;
    public const NO_CONTENT = 204;
    public const INTERNAL_SERVER_ERROR = 500;

}