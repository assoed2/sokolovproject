<?php
namespace App\Service;

use App\Exceptions\AppException;
use App\Responses\ProductResponse;
use App\HTTPCode\HTTPCode;
class View
{
    public function render(string $view, ProductResponse $response ): void
    {
        $viewPath = '../src/Views/' . $view . '.php';
            if (!file_exists($viewPath)) {
                throw new AppException('View not found', HTTPCode::INTERNAL_SERVER_ERROR);
            }
            http_response_code($response->code);
            header('Content-Type: application/json');
            require_once $viewPath;
    }
}