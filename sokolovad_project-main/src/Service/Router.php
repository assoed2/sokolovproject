<?php
namespace App\Service;

use App\Controllers\ProductController;
use App\Exceptions\AppException;
use App\Models\ProductModel;
use App\Validator\ProductValidator\ProductValidator;
use App\HTTPCode\HTTPCode;

class Router
{
    private array $routes = [];
    public ProductValidator $productValidator;
    public ProductModel $productModel;

    public View $view;
    public array $data;

    public function __construct(ProductValidator $productValidator, ProductModel $productModel, View $view, array $data)
    {
        $this->productValidator = $productValidator;
        $this->productModel = $productModel;
        $this->view = $view;
        $this->data = $data;
    }

    public function addRoute(string $pattern, string $method, string $controller, string $action):void
    {
        $this->routes[$pattern][$method] = ['controller' => $controller, 'action' => $action];
    }

    public function handleRequest(string $url, string $method):void
    {
        $isPageFound = false;
        foreach ($this->routes as $pattern => $methods) {

            if (preg_match($pattern, $url, $matches) && isset($methods[$method])) {
                $route = $methods[$method];
                $action = $route['action'];
                $controllerInstance = new ProductController($this->productValidator,$this->view,$this->productModel,$this->data);
                $controllerInstance->$action($matches[0]);
                $isPageFound = true;

            }

        }
        if(!$isPageFound){
            throw new AppException('Page not found',HTTPCode::NOT_FOUND);
        }

    }
}