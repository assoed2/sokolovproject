<?php
namespace App\Models;

use App\Entity\Product;
use App\Loaders\FileCSVLoader;
use App\Responses\ProductResponse;
use App\HTTPCode\HTTPCode;
class ProductModel
{
    public function getProductById(int $productId):ProductResponse{
        $productFound = false;
        $productData = array();
        $fileCSVLoader = new FileCSVLoader();
        $products = $fileCSVLoader->getDataFromFile('/dataCSV.csv');
        foreach ($products as $item){
            if($item['id']==$productId){
                $productFound = true;
                $productData[] = $item;
            }
        }

        $content = $productData;
        if (!$productFound) {
            return new ProductResponse(HTTPCode::NOT_FOUND, 'The specified resource was not found', $content);
        } else {
            return new ProductResponse(HTTPCode::GOOD_REQUEST, 'The specified resource was not found', $content);
        }
    }

    public function getUsersProducts(int $userId):ProductResponse{
        $content = array();
        $productFound = false;

        $fileCSVLoader = new FileCSVLoader();
        $products = $fileCSVLoader->getDataFromFile('/dataCSV.csv');
        foreach ($products as $item){
            if($item['user_id']==$userId){
                $productFound = true;
            }
        }

        if (!$productFound) {
            return new ProductResponse(HTTPCode::NOT_FOUND, 'The specified resource was not found', $content);
        } else {
            return new ProductResponse(HTTPCode::GOOD_REQUEST, 'The specified resource was not found', $content);
        }
    }
    public function getAllProducts():ProductResponse{
    $fileCSVLoader = new FileCSVLoader();
    $products = $fileCSVLoader->getDataFromFile('/dataCSV.csv');

        return new ProductResponse(HTTPCode::GOOD_REQUEST, 'Products returned',$products);
    }
    public function deleteProduct(int $productId):ProductResponse{
        $content = array();
        $productFound = false;
        $fileCSVLoader = new FileCSVLoader();
        $products = $fileCSVLoader->getDataFromFile('/dataCSV.csv');
        foreach ($products as $key => $item) {
            if ($item['id'] == $productId) {
                unset($products[$key]);
                $productFound = true;
            }
        }

        $fileCSVLoader->setDataInFile('/dataCSV.csv', $products);
        if (!$productFound) {
            return new ProductResponse(HTTPCode::NOT_FOUND, 'The specified resource was not found',$content);
        } else {
            return new ProductResponse(HTTPCode::NO_CONTENT, 'Products returned',$content);
        }
    }

    public function  createProduct(Product $product):ProductResponse {
        $data = array(
            'id'            =>  $product->id,
            'name'          =>  $product->name,
            'vendorCode'    =>  $product->vendorCode,
            'price'         =>  $product->price,
            'description'   =>  $product->description

        );
        $content = array();
        $fileCSVLoader = new FileCSVLoader();
        $existingData = $fileCSVLoader->getDataFromFile('/dataCSV.csv');
        $existingData[] = $data;
        $fileCSVLoader->setDataInFile('/dataCSV.csv', $existingData);

        return new ProductResponse(HTTPCode::CREATED, 'Product created',$content);
        }

    public function updateProduct(int $productId):ProductResponse{
        $content = array();
        $productFound = false;
        $fileCSVLoader = new FileCSVLoader();
        $products = $fileCSVLoader->getDataFromFile('/dataCSV.csv');
        foreach ($products as $key => $item){
            if($item['id'] == $productId){
                $productFound = true;
            }
        }

        if (!$productFound) {
            return new ProductResponse(HTTPCode::NOT_FOUND, 'The specified resource was not found',$content);
        } else {
            return new ProductResponse(HTTPCode::NO_CONTENT, 'Product updated',$content);
        }
    }
}