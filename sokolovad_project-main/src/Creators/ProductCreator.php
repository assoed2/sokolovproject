<?php
namespace App\Creators;

use Exception;
use App\Entity\Product;
use App\Exceptions\AppException;
use App\Validator\SchemeValidator\ProductSchemeValidator;

class ProductCreator
{
    private object $schemeValidator;

    public function __construct(
        ProductSchemeValidator $schemeValidator
    ){
        $this->schemeValidator = $schemeValidator;
    }
    public  function getProduct( array $data):Product{
            $this->schemeValidator->validateScheme($data);

            return new Product($data['id'],$data['name'],$data['vendorCode'],$data['price'],$data['description'],$data['warestockAmount'],$data['status']);
    }
}