<?php
namespace App\Loggers;

use App\Exceptions\AppException;

class ProductLogger
{
    public function logProductErrors(AppException $appException):void{
        $logFile = '../var/log/appError.log';
        $errorMessage = "[" . date('Y-m-d H:i:s') . "] " . $appException->getMessage() . " in " . $appException->getFile() . " on line " . $appException->getLine() . PHP_EOL;
        file_put_contents($logFile, $errorMessage, FILE_APPEND);
    }
}