<?php
namespace App\Controllers;

use App\Creators\ProductCreator;
use App\Exceptions\AppException;
use App\Models\ProductModel;
use App\Service\View;
use App\Validator\ProductValidator\ProductValidator;
use App\Validator\SchemeValidator\ProductSchemeValidator;

class ProductController
{

public ProductValidator $productValidator;
public ProductModel $productModel;
public View $view;
public array $data;
    public function __construct(ProductValidator $productValidator,View $view,ProductModel $productModel,array $data){
        $this->productValidator =   $productValidator ;
        $this->productModel =  $productModel;
        $this->view =  $view;
        $this->data = $data;
    }

    /**
     *@throws AppException
     */
    public function addProduct():void{
        $schemeValidator = new ProductSchemeValidator();
        $productCreator = new ProductCreator($schemeValidator);

        $product = $productCreator->getProduct($this->data);
        $this->productValidator->validateProduct($product);
        $response = $this->productModel->createProduct($product);
        $this->view->render('default',$response);
    }

    public function deleteProduct():void{
        $response = $this->productModel->deleteProduct($this->data['id']);
        $this->view->render('default',$response);
    }
    public function updateProduct():void{
        $response = $this->productModel->updateProduct($this->data['id'],$this->data);
        $this->view->render('default',$response);
    }

    public function getProductById():void{
        $response = $this->productModel->getProductById($this->data['id']);
        $this->view->render('default',$response);
    }
    public function getUsersProducts():void{
        $response = $this->productModel->getUsersProducts($this->data['user_id']);
        $this->view->render('default',$response);
    }

    public function getAllProducts():void{
        $response = $this->productModel->getAllProducts();
        $this->view->render('default',$response);
    }

}
