<?php

namespace App\Loaders;
class FileXMLLoader
{
    const VENDOR_CODE_INDEX = 'vendorCode';
    const PRICE_INDEX = 'price';
    const NAME_INDEX = 'name';
    const DESCRIPTION_INDEX = 'descritption';
    public function getDataFromFile($filePath):array{
        $filePath = PROJECT_ROOT.$filePath;
        $xmlStr = file_get_contents($filePath);
        $xmlFile = simplexml_load_string( $xmlStr);
        $products = array();

          foreach ($xmlFile->product as $productNode){
            $productXML = array();
            $productXML [self::VENDOR_CODE_INDEX]   = (string)$productNode->vendorCode;
            $productXML [self::PRICE_INDEX]         = (string)$productNode->price;
            $productXML [self::NAME_INDEX]          = (string)$productNode->name;
            $productXML [self::DESCRIPTION_INDEX]   = (string)$productNode->description;
          $products[] = $productXML;
          }
        return $products;
    }



}