<?php
namespace App\Validator\SchemeValidator;

use App\Exceptions\AppException;
use App\HTTPCode\HTTPCode;

class ProductSchemeValidator
{
    private array $requiredFields = ['name','vendorCode'];
    private array $acceptableFields = ['id','name','vendorCode','price','description','warestockAmount','status'];

    public function validateScheme(array $data):bool
    {
            if (empty($data)){
                throw new AppException('Empty input', HTTPCode::BAD_REQUEST);
            }

            foreach ($this->requiredFields as $field){
                if(!array_key_exists($field,$data)||empty($data[$field])){
                    throw new AppException('No required data', HTTPCode::BAD_REQUEST);
                }
            }

            foreach ($data as $key => $value){
                if(!in_array($key,$this->acceptableFields)){
                    throw new AppException('Unnecessary data', HTTPCode::BAD_REQUEST);
                }
            }

            return true;
    }
}

