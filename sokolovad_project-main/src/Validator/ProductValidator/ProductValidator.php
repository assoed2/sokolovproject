<?php
namespace App\Validator\ProductValidator;

use App\Entity\Product;
use App\Exceptions\AppException;
use App\HTTPCode;
class ProductValidator
{
    const MAX_NAME_LENGTH = 64;
    const MIN_NAME_LENGTH = 5;
    const MAX_DESCRIPTION_LENGTH = 300;
    const VENDOR_CODE_PATTERN = '/^\d{1,3}-\d{1,}$/';
    const  PRICE_PATTERN = '/^\d+,\d{2}$/';
    const MAX_VENDOR_CODE_LENGTH = 10;

    public function validateProduct(Product $product):bool{
        $this->validateVendorCode($product->vendorCode);
        $this->validatePrice($product->price);
        $this->validateName($product->name);
        $this->validateDescription($product->description);
        $this->validateWarestockAmount($product->warestockAmount);
        $this->validateStatus($product->status);
        return true;
    }

    public function validateName($name): bool{
        if (!(is_string($name) && mb_strlen($name) >= self::MIN_NAME_LENGTH && mb_strlen($name) <= self::MAX_NAME_LENGTH)) {
            throw new AppException('Name is not correct', HTTPCode\HTTPCode::BAD_REQUEST);
        }

        return true;
    }

    public function validateDescription(string $description):bool{
        if (!(strlen($description) <= self::MAX_DESCRIPTION_LENGTH)) {
            throw new AppException('Description is not correct', HTTPCode\HTTPCode::BAD_REQUEST);
        }

        return true;
    }

    public function validateVendorCode($vendorCode): bool{
        if (!(preg_match(self::VENDOR_CODE_PATTERN, $vendorCode) && mb_strlen($vendorCode) <= self::MAX_VENDOR_CODE_LENGTH)) {
            throw new AppException('Vendor code is not correct', HTTPCode\HTTPCode::BAD_REQUEST);
        }

        return true;
    }

    public function validatePrice(string $price): bool{
        if (!preg_match(self::PRICE_PATTERN, $price)) {
            throw new AppException('Price is not correct', HTTPCode\HTTPCode::BAD_REQUEST);
        }

        return true;
    }

    public function validateStatus(int $status): bool{
        if (!($status==0||$status==1)) {
            throw new AppException('Status is not correct', HTTPCode\HTTPCode::BAD_REQUEST);
        }

        return true;
    }

    public function validateWarestockAmount(int $warestockAmount): bool{
        if ($warestockAmount<0) {
            throw new AppException('Warestock amount is not correct', HTTPCode\HTTPCode::BAD_REQUEST);
        }

        return true;
    }



}

