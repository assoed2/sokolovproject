## Проверка `Code`, `Price`, `Name`, `Description`, `Status` и `warestockAmount` на валидность

### Предусловие
1. Приложение запущено на сервере nginx
2. Приложение принимает запросы от POSTMAN

### Шаги тест-кейса
1. Сформировать POST запрос вида: http://localhost/products/
2. В разделе `Body` указать следующие параметры (`code`, `price`, `name`, `description`, `warestockAmount`, `status`)
3. Последовательно изменять значения полей согласно тестовым данным

| №  | Функция                               | Условие начала проверки  | Тестовые наборы данных                                      | Ожидаемый результат      |
|----|----------------------------------------|-------------------------|-----------------------------------------------------------|-------------------------|
| 1  | testNameValidator($name, $expectedResult)    | Имя слишком короткое    | 'adf'                                                     | Ошибка: Name is not correct |
| 2  | testNameValidator($name, $expectedResult)    | Имя слишком длинное     | 'qwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiop' | Ошибка: Name is not correct |
| 3  | testNameValidator($name, $expectedResult)    | Валидное имя           | 'validname'                                               | Успех                   |
| 4  | testVendorCodeValidator($vendorCode, $expectedResult) | Пустой vendorCode  | ''                                                        | Ошибка: Vendor code is not correct |
| 5  | testVendorCodeValidator($vendorCode, $expectedResult) | Слишком короткий vendorCode | '-122243'                                           | Ошибка: Vendor code is not correct |
| 6  | testVendorCodeValidator($vendorCode, $expectedResult) | Слишком длинный vendorCode | '2324-12224'                                      | Ошибка: Vendor code is not correct |
| 7  | testVendorCodeValidator($vendorCode, $expectedResult) | Слишком длинное тело vendorCode | '232-1222443'                                   | Ошибка: Vendor code is not correct |
| 8  | testVendorCodeValidator($vendorCode, $expectedResult) | Слишком короткое тело vendorCode | '232-'                                       | Ошибка: Vendor code is not correct |
| 9  | testVendorCodeValidator($vendorCode, $expectedResult) | Не соответствует шаблону | '23245345'                                           | Ошибка: Vendor code is not correct |
| 10 | testVendorCodeValidator($vendorCode, $expectedResult) | Валидный vendorCode | '232-123123'                                        | Успех                   |
| 11 | testDescriptionValidator($description, $expectedResult) | Слишком длинное описание | 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr...' (более 300 символов) | Ошибка: Description is not correct |
| 12 | testDescriptionValidator($description, $expectedResult) | Валидное описание   | 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr' | Успех                   |
| 13 | testPriceValidator($price, $expectedResult) | Слишком длинная цена | '12,455'                                                 | Ошибка: Price is not correct |
| 14 | testPriceValidator($price, $expectedResult) | Не соответствует шаблону | '12'                                                  | Ошибка: Price is not correct |
| 15 | testPriceValidator($price, $expectedResult) | Цена содержит буквы | '23,5f'                                               | Ошибка: Price is not correct |
| 16 | testPriceValidator($price, $expectedResult) | Валидная цена        | '23,51'                                                 | Успех                   |
| 17 | testStatusValidator($status, $expectedResult) | Неверный статус    |  2                                                 | Ошибка: Status is not correct |
| 18 | testStatusValidator($status, $expectedResult) | Верный статус       | 0, 1                                                    | Успех                   |
| 19 | testWarestockAmountValidator($warestockAmount, $expectedResult) | Отрицательное значение | -2          | Ошибка: Warestock amount is not correct |
| 20 | testWarestockAmountValidator($warestockAmount, $expectedResult) | Валидное значение     | 224          | Успех                   |