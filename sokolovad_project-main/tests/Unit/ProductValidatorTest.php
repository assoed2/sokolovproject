<?php
namespace Unit;
require_once  __DIR__.'/../../vendor/autoload.php';

use App\Exceptions\AppException;
use App\Validator\ProductValidator\ProductValidator;

class ProductValidatorTest extends \Codeception\Test\Unit
{


    private ProductValidator $validator;
    protected function _before() {
        $this->validator = new ProductValidator();
    }


    static function getNameCasesProvider():array{
        return [
            'When name is too short'    =>  [
                'name'  => 'adf',
                'expectedResult' => false
            ],
            'When name is too long'    =>  [
                'name'  => 'qwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiop',
                'expectedResult' => false
            ],
            'When name is valid'    =>  [
                'name'  => 'validname',
                'expectedResult' => true,
            ],
        ];
    }

    /**
     * @dataProvider getNameCasesProvider
     */

    public function testNameValidator(string $name, bool $expectedResult):void{
        if (!$expectedResult) {
            $this->expectException(AppException::class);
            $this->expectExceptionMessage('Name is not correct');
        }

            $this->assertTrue($this->validator->validateName($name));
    }


    static function getVendorCodeCasesProvider():array{
        return [
            'When vendorCode is Empty'    =>  [
                'vendorCode'  => '',
                'expectedResult' => false
            ],
            'When vendorCodePrefix is too short'    =>  [
                'vendorCode'  => '-122243',
                'expectedResult' => false
            ],
            'When vendorCodePrefix is too long'    =>  [
                'vendorCode'  => '2324-12224',
                'expectedResult' => false
            ],
            'When vendorCode is too long'    =>  [
                'vendorCode'  => '232-1222443',
                'expectedResult' => false
            ],
            'When vendorCodeBody is too short'    =>  [
                'vendorCode'  => '232-',
                'expectedResult' => false
            ],
            'When vendorCode is not match pattern '    =>  [
                'vendorCode'  => '23245345',
                'expectedResult' => false
            ],
            'When vendorCode is valid'    =>  [
                'vendorCode'  => '232-123123',
                'expectedResult' => true
            ],


        ];
    }

    /**
     * @dataProvider getVendorCodeCasesProvider
     */
    public function testVendorCodeValidator(string $vendorCode, bool $expectedResult):void{
        if (!$expectedResult) {
            $this->expectException(AppException::class);
            $this->expectExceptionMessage('Vendor code is not correct');
        }

            $this->assertTrue($this->validator->validateVendorCode($vendorCode));

    }

    static function getDescriptionCasesProvider():array{
        return [
            'When description is Too Long'    =>  [
                'description'  => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,no sea takimata sanctus est Lorem ipsum dolor sit amet. Loremna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,      no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ',
                'expectedResult' => false
            ],
            'When description is valid'    =>  [
                'description'  => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr',
                'expectedResult' => true
            ],

        ];
    }
    /**
     * @dataProvider getDescriptionCasesProvider
     */

    public function testDescriptionValidator(string $description, bool $expectedResult):void{
       if (!$expectedResult) {
            $this->expectException(AppException::class);
            $this->expectExceptionMessage('Description is not correct');
       }

            $this->assertTrue($this->validator->validateDescription($description));
    }


    static function getPriceCasesProvider():array{
        return [
            'When second part of price is too long'    =>  [
                'price'  => '12,455',
                'expectedResult' => false
            ],

            'When  price is not match pattern '    =>  [
                'price'  => '12',
                'expectedResult' => false
            ],
            'When price has letters'    =>  [
                'price'  => '23,5f',
                'expectedResult' => false
            ],
            'When price is valid'    =>  [
                'price'  => '23,51',
                'expectedResult' => true
            ],

        ];
    }

    /**
     * @dataProvider getPriceCasesProvider
     */

    public function testPriceValidator(string $price, bool $expectedResult):void{
          if (!$expectedResult) {
              $this->expectException(AppException::class);
              $this->expectExceptionMessage('Price is not correct');
          }

              $this->assertTrue($this->validator->validatePrice($price));
    }

    static function getStatusCasesProvider():array{
        return [
            'When status is not 0 or 1'    =>  [
                'status'  => 2,
                'expectedResult' => false
            ],
            'When status is valid'    =>  [
                'status'  => 1,
                'expectedResult' => true
            ],

        ];
    }

    /**
     * @dataProvider getStatusCasesProvider
     */

    public function testStatusValidator(int $status, bool $expectedResult):void
    {
        if (!$expectedResult) {
            $this->expectException(AppException::class);
            $this->expectExceptionMessage('Status is not correct');
        }

            $this->assertTrue($this->validator->validateStatus($status));
    }
    static function getWarestockAmountCasesProvider():array{
        return [
            'When warestockAmount is negative'    =>  [
                'warestockAmount'  => -2,
                'expectedResult' => false
            ],
            'When warestockAmount is valid'    =>  [
                'warestockAmount'  => 224,
                'expectedResult' => true
            ],

        ];
    }

    /**
     * @dataProvider getWarestockAmountCasesProvider
     */

    public function testWarestockAmountValidator(int $warestockAmount, bool $expectedResult)
    {
        if (!$expectedResult) {
            $this->expectException(AppException::class);
            $this->expectExceptionMessage('Warestock amount is not correct');
        }
            $this->assertTrue($this->validator->validateWarestockAmount($warestockAmount));
    }

}