<?php
require_once '../vendor/autoload.php';

use App\Exceptions\AppException;
use App\Loggers\ProductLogger;
use App\Models\ProductModel;
use App\Service\Router;
use App\Service\View;
use App\Validator\ProductValidator\ProductValidator;

$queryString = file_get_contents('php://input');
parse_str($queryString,$data);
$productValidator = new ProductValidator();
$productModel = new ProductModel();
$view = new View();
$router = new Router($productValidator,$productModel,$view,$data);
$router->addRoute('#^/products/(\d+)#', 'GET', 'ProductController', 'getProductById');
$router->addRoute('#^/products/(\d+)#', 'PUT', 'ProductController', 'updateProduct');
$router->addRoute('#^/products/(\d+)#', 'PATCH', 'ProductController', 'updateProduct');
$router->addRoute('#^/products/(\d+)#', 'DELETE', 'ProductController', 'deleteProduct');
$router->addRoute('#^/products/$#', 'POST', 'ProductController', 'addProduct');
$router->addRoute('#^/products/$#', 'GET', 'ProductController', 'getAllProducts');
$router->addRoute('#^/products/my$#', 'GET', 'ProductController', 'getUsersProducts');

$currentUrl = $_SERVER['REQUEST_URI'];
$method = $_SERVER['REQUEST_METHOD'];

try {
    $router->handleRequest($currentUrl, $method);
}
    catch (AppException $e){
        $productLogger = new ProductLogger();
        $productLogger->logProductErrors($e);
}


